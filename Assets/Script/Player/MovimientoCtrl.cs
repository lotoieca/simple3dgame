using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class MovimientoCtrl : MonoBehaviour
{
    public float velocidadMovimiento;
    private Rigidbody rig;

    // Start is called before the first frame update
    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float horizontalMove = 0;
        float verticalMove = 0;
        if (Input.GetAxis("Horizontal") != 0)
        {
            horizontalMove = Input.GetAxis("Horizontal") * velocidadMovimiento;
        }
        if(Input.GetAxis("Vertical") != 0)
        {
            verticalMove = Input.GetAxis("Vertical") * velocidadMovimiento;
        }

        rig.AddForce(new Vector3(horizontalMove, 0,verticalMove));
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}

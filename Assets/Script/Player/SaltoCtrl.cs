using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]
public class SaltoCtrl : MonoBehaviour
{
    public float fuerzaSalto;
    private Rigidbody rig;
    private bool estaEnTierra = false;
    private AudioSource audioSalto;
    private int saltosRestantes = 2;

    // Start is called before the first frame update
    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody>();
        audioSalto = gameObject.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump") && saltosRestantes > 0)
        {
            rig.AddForce(new Vector3(0, fuerzaSalto,0));
            audioSalto.Play();
            estaEnTierra = false;
            saltosRestantes--;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Plataforma" && !estaEnTierra)
        {
            estaEnTierra = true;
            saltosRestantes = 2;
        }
    }
}

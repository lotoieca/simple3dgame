using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(SphereCollider))]
public class ReaparecerCtrl : MonoBehaviour
{
    private Rigidbody rig;
    private int vidas = 1;
    // Start is called before the first frame update
    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Respawn")
        {
            Debug.Log("trigger activado");
            vidas = 1;
            resetPosition();
        }
    }
    // Update is called once per frame
    public void resetPosition()
    {
        vidas--;
        if (vidas == 0)
        {
            transform.position = new Vector3(0, 0, 0);
            rig.velocity = Vector3.zero;
            rig.angularVelocity = Vector3.zero;
            vidas = 1;
        }
    }
}

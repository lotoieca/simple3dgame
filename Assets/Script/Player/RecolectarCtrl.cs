using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(MeshCollider))]
public class RecolectarCtrl : MonoBehaviour
{
    private AudioSource sonidoRecoleccion;
    private int contMonedas = 0;
    private Text contMonedaUI;
    private int maxMonedas;
    // Start is called before the first frame update
    void Start()
    {
        sonidoRecoleccion = gameObject.GetComponent<AudioSource>();
        contMonedaUI = GameObject.Find("ContadorUI").GetComponent<Text>();
        maxMonedas = GameObject.FindGameObjectsWithTag("Moneda").Length;

        contMonedaUI.text = "Monedas: " + contMonedas + "/" + maxMonedas;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "Moneda")
        {
            Destroy(collision.gameObject);
            sonidoRecoleccion.Play();
            contMonedas++;
            Debug.Log("MONEDAS RECOGIDAS: "+contMonedas);
            contMonedaUI.text = "Monedas: " + contMonedas + "/" + maxMonedas;
        }
    }
}

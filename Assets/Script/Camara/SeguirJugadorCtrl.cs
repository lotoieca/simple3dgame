using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguirJugadorCtrl : MonoBehaviour
{
    public Transform objetivo;
    public float yOffset;
    // Start is called before the first frame update
    void Update()
    {
        transform.position = new Vector3(objetivo.position.x, 6, objetivo.position.z + yOffset);
    }
}
